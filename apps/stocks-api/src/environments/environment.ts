// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  token: 'Tsk_06b909bad03a4283b711623c35191783',
  apiURL: 'https://sandbox.iexapis.com/',
};
