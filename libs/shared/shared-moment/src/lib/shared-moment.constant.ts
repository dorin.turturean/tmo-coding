import { InjectionToken } from '@angular/core';
import { Moment } from 'moment';

export const SharedMoment = new InjectionToken<Moment>('SharedMoment');
