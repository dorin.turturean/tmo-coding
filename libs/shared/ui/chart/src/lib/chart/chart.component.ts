import { ChangeDetectionStrategy, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'coding-challenge-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChartComponent implements OnInit, OnChanges {
  @Input() data: Array<Array<string | number>> = [];

  chartData$: BehaviorSubject<Array<Array<string | number>>> = new BehaviorSubject([]);
  chart: {
    title: string;
    type: string;
    data: any;
    columnNames: string[];
    options: any;
  };

  ngOnChanges(changes: SimpleChanges): void {
    const { data } = changes;

    if (data) {
      this.chartData$.next(data.currentValue);
    }
  }

  ngOnInit() {
    this.chart = {
      title: '',
      type: 'LineChart',
      data: [],
      columnNames: ['period', 'close'],
      options: { title: `Stock price`, width: '600', height: '400' }
    };
  }
}
