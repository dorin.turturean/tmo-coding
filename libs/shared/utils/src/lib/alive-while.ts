import { Subject } from 'rxjs';

class AliveWhile extends Subject<any> {

  public destroy() {
    this.next();
    this.complete();
  }
}

export function aliveWhile(): AliveWhile {
  return new AliveWhile();
}
