import * as H2o2 from '@hapi/h2o2';
import * as axios from 'axios';
import { AxiosRequestConfig } from 'axios';
import { Server } from 'hapi';
import * as querystring from 'querystring';
import { environment } from '../../../apps/stocks-api/src';

const hours24 = 60 * 60 * 24 * 1000;

export async function register(server: Server) {
  await server.register(H2o2);

  const stockCache = server.cache({
    expiresIn: hours24,
    segment: 'stockSegment'
  });

  server.route({
    method: 'GET',
    path: '/api/beta/stock/{symbol}/chart/date/{date}',
    handler: async request => {
      const { token } = request.query;
      const { symbol, date } = request.params;
      const id = `${date}`;
      const cacheValue = await stockCache.get(id);

      if (!cacheValue) {
        const query = querystring.stringify({ token });
        const url = `${environment.apiURL}beta/stock/${symbol}/chart/date/${date}?${query}`;
        const config: AxiosRequestConfig = { url };
        const {data} = await axios.default(config);

        stockCache.set(id, data);
      }

      return cacheValue;
    }
  });
}

