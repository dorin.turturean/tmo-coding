module.exports = {
  name: 'stocks-data-access-interceptor',
  preset: '../../../jest.config.js',
  coverageDirectory: '../../../coverage/libs/stocks/data-access-interceptor',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
