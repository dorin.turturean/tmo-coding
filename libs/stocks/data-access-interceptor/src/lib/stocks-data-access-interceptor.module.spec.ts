import { async, TestBed } from '@angular/core/testing';
import { StocksDataAccessInterceptorModule } from './stocks-data-access-interceptor.module';

describe('StocksDataAccessInterceptorModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [StocksDataAccessInterceptorModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(StocksDataAccessInterceptorModule).toBeDefined();
  });
});
