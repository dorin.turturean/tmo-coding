import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StocksDataAccessInterceptor } from './stocks-data-access.interceptor';

@NgModule({
  imports: [CommonModule],
  providers: [
    StocksDataAccessInterceptor,
  ]
})
export class StocksDataAccessInterceptorModule {}
