import { Inject, Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { startsWith } from 'lodash-es';

import { StocksAppConfig, StocksAppConfigToken } from '@coding-challenge/stocks/data-access-app-config';

@Injectable()
export class StocksDataAccessInterceptor implements HttpInterceptor {

  constructor(@Inject(StocksAppConfigToken) private env: StocksAppConfig) {
  }

  apiRequest(req: HttpRequest<any>): HttpRequest<any> {
    const headers: any = {
      'Content-Type': 'application/json'
    };

    const newParams = req.params.set('token', this.env.apiKey);

    return req.clone({
      setHeaders: headers,
      params: newParams
    });
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const { url } = req;
    const apiRequest = startsWith(url, '/api/beta/stock');

    if (apiRequest) {
      req = this.apiRequest(req);
    }

    return next.handle(req);
  }
}
