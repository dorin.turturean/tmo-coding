import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { StocksAppConfig, StocksAppConfigToken } from '@coding-challenge/stocks/data-access-app-config';
import { Effect } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/nx';
import { map } from 'rxjs/operators';
import { FetchPriceQuery, PriceQueryActionTypes, PriceQueryFetched, PriceQueryFetchError } from './price-query.actions';
import { PriceQueryPartialState } from './price-query.reducer';
import { PriceQueryResponse } from './price-query.type';
import { range, reduce } from 'lodash-es';
import { forkJoin } from 'rxjs';
import { SharedMoment, SharedMomentType } from '@coding-challenge/shared/shared-moment';

@Injectable()
export class PriceQueryEffects {
  @Effect() loadPriceQuery$ = this.dataPersistence.fetch(
    PriceQueryActionTypes.FetchPriceQuery,
    {
      run: (action: FetchPriceQuery, state: PriceQueryPartialState) => {
        const { to, from, symbol } = action;
        const diffDays = this.sharedMoment(to).diff(this.sharedMoment(from), 'days') + 1;

        const stockRange = range(diffDays).map((index) => {
          const date = this.sharedMoment(from).add(index, 'days').format('YYYYMMDD');
          return this.httpClient.get(`/api/beta/stock/${symbol}/chart/date/${date}`);
        });

        return forkJoin(stockRange)
          .pipe(
            map(resp => {
              const prices = reduce(resp, (accumulator, val: PriceQueryResponse[]) => {
                return [...accumulator, ...val];
              }, []);

              return new PriceQueryFetched(prices as PriceQueryResponse[]);
            })
          );
      },

      onError: (action: FetchPriceQuery, error) => {
        return new PriceQueryFetchError(error);
      }
    }
  );

  constructor(
    @Inject(StocksAppConfigToken) private env: StocksAppConfig,
    @Inject(SharedMoment) private sharedMoment: SharedMomentType,
    private httpClient: HttpClient,
    private dataPersistence: DataPersistence<PriceQueryPartialState>
  ) {
  }
}
