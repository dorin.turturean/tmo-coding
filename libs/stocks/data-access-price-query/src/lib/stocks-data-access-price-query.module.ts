import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { PriceQueryEffects } from './+state/price-query.effects';
import { PriceQueryFacade } from './+state/price-query.facade';
import { PRICEQUERY_FEATURE_KEY, priceQueryReducer } from './+state/price-query.reducer';
import * as moment from 'moment';
import { SharedMoment } from '@coding-challenge/shared/shared-moment';
import { StocksDataAccessInterceptor } from '@coding-challenge/stocks/data-access-interceptor';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    StoreModule.forFeature(PRICEQUERY_FEATURE_KEY, priceQueryReducer),
    EffectsModule.forFeature([PriceQueryEffects])
  ],
  providers: [
    PriceQueryFacade,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: StocksDataAccessInterceptor,
      multi: true
    },
    {
      provide: SharedMoment,
      useValue: moment
    }
  ]
})
export class StocksDataAccessPriceQueryModule {
}
