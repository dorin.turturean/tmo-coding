import { StocksVmService } from './stocks.vm.service';
import { FormBuilder } from '@angular/forms';
import moment from 'moment';

describe('StocksVmService', () => {
  const stocksVmService = new StocksVmService(new FormBuilder(), moment);

  it('#create() should be return new form', () => {
    const formGroup = stocksVmService.create();

    expect(formGroup).not.toBeNull();
  });

  it('check default values for stack form', () => {
    const testValue = { symbol: '23', from: new Date(), to: new Date() };
    const formGroup = stocksVmService.create(testValue);

    expect(formGroup.value.symbol).toBe(testValue.symbol);
    expect(formGroup.value.from).toBe(testValue.from);
    expect(formGroup.value.to).toBe(testValue.to);
  });
});
