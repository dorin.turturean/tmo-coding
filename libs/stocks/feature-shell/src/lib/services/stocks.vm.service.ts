import { Inject, Injectable } from '@angular/core';
import { FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { SharedMoment, SharedMomentType } from '@coding-challenge/shared/shared-moment';

export class StockForm {
  symbol: string = null;
  from: Date = null;
  to: Date = null;

  constructor(option?) {
    const defaultValue = [''];

    Object.keys(this)
      .forEach((propertyKey, index) => {
        const control = (option && option[propertyKey]) || defaultValue;

        this[propertyKey] = control;
      });
  }
}

@Injectable()
export class StocksVmService {
  options: Partial<Pick<any, keyof StockForm>>;

  constructor(
    private fb: FormBuilder,
    @Inject(SharedMoment) private sharedMoment: SharedMomentType,
  ) {
    this.options = {
      symbol: [null, Validators.required],
      from: [null, Validators.required],
      to: [null, Validators.required]
    };
  }

  create(values: Partial<StockForm> = null): FormGroup {
    const stockForm = new StockForm(this.options);
    const stockFormGroup = this.fb.group(stockForm, {
      validators: [this.validateIntervalDates('from', 'to')]
    });

    if (values) {
      stockFormGroup.patchValue(values);
    }

    return stockFormGroup;
  }

  validateIntervalDates(from: string, to: string) {

    return (group: FormGroup): ValidationErrors | null => {
      const fromControl = group.controls[from];
      const toControl = group.controls[to];

      if (group.dirty && fromControl && toControl) {
        const toDate = this.sharedMoment(toControl.value as Date);
        const fromDate = this.sharedMoment(fromControl.value as Date);

        if(toDate.isValid() && fromDate.isValid() && fromDate.isAfter(toDate)) {
          return {rangeDateIsInvalid:  true}
        }
      }
      return null;
    };
  }
}
