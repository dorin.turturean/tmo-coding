import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  MatButtonModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatInputModule,
  MatNativeDateModule,
  MatSelectModule
} from '@angular/material';
import { SharedUiChartModule } from '@coding-challenge/shared/ui/chart';
import { StocksComponent } from './stocks/stocks.component';
import { ReactiveFormsModule } from '@angular/forms';
import { StocksVmService } from './services/stocks.vm.service';
import { StocksFeatureShellRoutingModule } from './stocks-feature-shell-routing.module';

@NgModule({
  imports: [
    CommonModule,
    StocksFeatureShellRoutingModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    SharedUiChartModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],
  declarations: [StocksComponent],
  providers: [
    StocksVmService
  ],
  exports: [StocksComponent]
})
export class StocksFeatureShellModule {
}
