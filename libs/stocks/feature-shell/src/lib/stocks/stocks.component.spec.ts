import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StocksComponent } from './stocks.component';
import {
  MatDatepickerModule,
  MatFormFieldModule,
  MatInputModule,
  MatNativeDateModule,
  MatSelectModule
} from '@angular/material';
import { SharedUiChartModule } from '@coding-challenge/shared/ui/chart';
import { RouterTestingModule } from '@angular/router/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { StockForm, StocksVmService } from '../services/stocks.vm.service';
import { MockStocksVmService } from '../mocks/mock-stocks.vm.service';
import { MockPriceQueryFacade } from '../mocks/mock-price-query.facade';
import { PriceQueryFacade } from '@coding-challenge/stocks/data-access-price-query';
import { of, Subject } from 'rxjs';

describe('StocksComponent', () => {
  let component: StocksComponent;
  let fixture: ComponentFixture<StocksComponent>;

  const mockStocksVmService = new MockStocksVmService();
  const priceQueryFacade = new MockPriceQueryFacade();

  beforeEach(async(() => {
    mockStocksVmService.create.mockRejectedValueOnce(new FormBuilder().group(new StockForm()));
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        NoopAnimationsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        SharedUiChartModule,
        MatDatepickerModule,
        MatNativeDateModule
      ],
      declarations: [StocksComponent],
      providers: [
        {
          provide: StocksVmService,
          useValue: mockStocksVmService
        },
        {
          provide: PriceQueryFacade,
          useValue: priceQueryFacade
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StocksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
