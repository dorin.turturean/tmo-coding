import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { PriceQueryFacade } from '@coding-challenge/stocks/data-access-price-query';
import { StockForm, StocksVmService } from '../services/stocks.vm.service';
import { Observable, Subject } from 'rxjs';
import { debounceTime, takeUntil } from 'rxjs/operators';
import { aliveWhile } from '@coding-challenge/shared/utils';

@Component({
  selector: 'coding-challenge-stocks',
  templateUrl: './stocks.component.html',
  styleUrls: ['./stocks.component.css']
})
export class StocksComponent implements OnDestroy, OnInit {
  alive$ = aliveWhile();
  stockPickerForm: FormGroup;
  quotes$ = this.priceQuery.priceQueries$;
  symbolChange$: Subject<string> = new Subject();
  maxDate = new Date();

  constructor(private stocksVmService: StocksVmService,
              private priceQuery: PriceQueryFacade
  ) {
    this.stockPickerForm = this.stocksVmService.create()
  }

  formChangeStatus(status) {
    if (status === 'INVALID' && this.stockPickerForm.hasError('rangeDateIsInvalid')) {
      const { from, to } = this.stockPickerForm.value as StockForm;
      const defaultDate = from || to || new Date();

      this.stockPickerForm.patchValue({ to: defaultDate, from: defaultDate });
    }
  }

  symbolChanged(newSymbol) {
    this.symbolChange$.next(newSymbol);
  }

  fetchQuote() {
    if (this.stockPickerForm.valid) {
      const { symbol, to, from } = this.stockPickerForm.value as StockForm;
      this.priceQuery.fetchQuote(symbol, from, to);
    }
  }

  ngOnInit(): void {
    this.symbolChange$
      .pipe(
        takeUntil(this.alive$),
        debounceTime(200)
      )
      .subscribe(this.fetchQuote.bind(this));

    this.stockPickerForm.statusChanges
      .pipe(
        takeUntil(this.alive$)
      )
      .subscribe(this.formChangeStatus.bind((this)));
  }

  ngOnDestroy(): void {
    this.alive$.destroy();
  }
}
